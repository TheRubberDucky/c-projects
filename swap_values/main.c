#include <stdio.h>
#include <stdlib.h>

int main()
{
    int num1 = 9;
    int num2 = 10;
    int *p_num1 = &num1; //pointers store the address of another variable
    int *p_num2 = &num2;
    int tmp;

    //before the swap
    printf("num 1: %i\n", num1);
    printf("num 2: %i\n\n", num2);


    //create the swap
    tmp = num1;
    *p_num1 = num2; //this will take what is in the address of num1 and replace it with num2
    *p_num2 = tmp;

    //after the swap
    printf("num 1: %i\n", num1);
    printf("num 2: %i\n\n", num2);   

    /*
    - if you want the address of a pointer you print using the %p flag: &p_num1
    - if you want the value in the address of a pointer then you put the * in front of the variable
      name and print using something other then %p for example %d
    */
    printf("this is the address of the integer: %p\n", p_num1);
    printf("this is the value in the address of the integer: %d", *p_num1);
    return 0;
}
